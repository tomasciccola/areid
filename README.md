# A.R.E.I.D. 
## ~ Automáta repetidor efímero de información distribuída ~

Pequeño servidor que corre un [Discovery Swarm Web](https://github.com/RangerMauve/discovery-swarm-web).
El mismo reúne las propiedades de un signalhub (servidor que coordina conexiones p2p entre clientes) y un gateway (básicamente cada cliente se comunica vía websocket con este server que repite la data a todo cliente conectado)
