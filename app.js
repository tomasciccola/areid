const fs = require('fs')
const choo = require('choo')
const html = require('choo/html')
const raw = require('choo/html/raw')
const css = require('sheetify')
const marked = require('marked')
const text = fs.readFileSync('./README.md').toString()

css('tachyons')

const app = choo()

app.use((state, emitter) => {
})

app.route('/', (state, emit) => {
  return html`
  <body class="mw6 pv6 mw7-ns center code lh-copy bg-black-90 white-90">
    <main class="pa5 tc">
      ${raw(marked(text))}
    </main>
  </body>
  `
})

app.mount('body')
