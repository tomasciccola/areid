const http = require('http')
const fs = require('fs')
const DSS = require('discovery-swarm-web/server')
const PORT = 3472
const root = fs.readFileSync('public/index.html').toString()
const js = fs.readFileSync('public/bundle.js').toString()

const server = http.createServer((req, res) => {
  switch (req.url) {
    case '/':
      res.writeHead(200, {
        'Content-Type': 'text/html'
      })
      res.end(root)
      break
    case '/bundle.js':
      res.writeHead(200, {
        'Content-Type': 'text/js'
      })
      res.end(js)
      break
  }
})

var portal = DSS.createServer({ server })
portal._discovery.on('connection', (info) => {
})
server.listen(PORT)
